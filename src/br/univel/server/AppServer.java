package br.univel.server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import br.unviel.common.ChatClient;
import br.unviel.common.ChatServer;

public class AppServer implements ChatServer{
	
	private Map<String, ChatClient> mapClientes = new HashMap<>();
	
	@Override
	public void registrar(String nome, ChatClient client) throws RemoteException {
		
		if(mapClientes.containsKey(nome)) {
			throw new RemoteException("Esse nome já está sendo utilizado.");
		}
		
		mapClientes.put(nome, client);
	}

	@Override
	public void enviarMsgPrivada(String from, String to, String msg) throws RemoteException {
		mapClientes.get(to).receberMsgPrivada(from, msg);
		System.out.println(mapClientes.get("Luciano"));
	}

	@Override
	public void enviarMsgPublica(String from, String msg) throws RemoteException {
		
		for(Entry<String, ChatClient> e: mapClientes.entrySet()) {
			if( ! e.getKey().equals(from)) {
				continue;
			} else {
				e.getValue().receberMsgPublica(from, msg);
			}
		}
	}

	@Override
	public void logoff(String who) throws RemoteException {
		mapClientes.remove(who).notificarSaida(who);		
	}
	
	public static void main(String[] args) {
		System.out.println("Iniciando o server...");

		AppServer appServer = new AppServer();
		ChatServer chatServer;
		try {
			chatServer = (ChatServer) UnicastRemoteObject.exportObject(appServer, 0);
			Registry registry = LocateRegistry.createRegistry(1818);
			registry.rebind(ChatServer.NOME, chatServer);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	
	}
	
	
	
}
