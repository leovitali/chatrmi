package br.univel.client;

import java.io.Serializable;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import br.unviel.common.ChatClient;
import br.unviel.common.ChatServer;

public class AppClient implements ChatClient, Serializable{

	@Override
	public void receberMsgPrivada(String from, String msg) throws RemoteException {
		System.out.println(from + " (Privada): " + msg);
	}

	@Override
	public void receberMsgPublica(String from, String msg) throws RemoteException {
		System.out.println(from + ": " + msg);
	}

	@Override
	public void notificarEntrada(String nome) throws RemoteException {
		System.out.println(nome + " está online.");
	}

	@Override
	public void notificarSaida(String nome) throws RemoteException {
		System.out.println(nome + " está offline.");
	}
	
	
	public static void main(String[] args) {
		Registry registry;
		try {
			registry = LocateRegistry.getRegistry("", 1818);
			ChatServer chatServer = (ChatServer) registry.lookup(ChatServer.NOME);
			
			ChatClient chatClient = new AppClient();
			chatServer.registrar("Leonardo", chatClient);
			chatServer.registrar("Luciano", chatClient);
			
			chatServer.enviarMsgPrivada("Leonardo", "Luciano", "funciona cuzão");
			
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
	}
}
