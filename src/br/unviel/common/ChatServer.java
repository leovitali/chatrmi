package br.unviel.common;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ChatServer extends Remote {
	
	public static final String NOME = "CHATSERVER";
	
	public void registrar(String nome, ChatClient client) throws RemoteException;
	
	public void enviarMsgPrivada(String from, String to, String msg) throws RemoteException;
	
	public void enviarMsgPublica(String from, String msg) throws RemoteException;
	
	public void logoff(String who) throws RemoteException;
	
}
