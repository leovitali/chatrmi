package br.unviel.common;

import java.rmi.RemoteException;

public interface ChatClient {
	
	public void receberMsgPrivada(String from, String msg) throws RemoteException;
	
	public void receberMsgPublica(String from, String msg) throws RemoteException;
	
	public void notificarEntrada(String nome) throws RemoteException;
	
	public void notificarSaida(String nome) throws RemoteException;
	
}
